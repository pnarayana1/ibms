import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by 161744 on 10/12/2015.
 */
public class ExportSchedule {

    @Test
    public void ibmsLogin() throws Exception {
        org.sikuli.basics.Debug.setDebugLevel(3);
        AssingProgrammes ep = new AssingProgrammes();
        String new_ep = ep.Episode;
        System.out.println(new_ep);

        Calendar calendar1 = Calendar.getInstance();
        calendar1.add(Calendar.YEAR, 2);
        Date tomorrow1 = calendar1.getTime();
        SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
        String tomorrowsdate1 = formatter1.format(tomorrow1);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.add(Calendar.DAY_OF_YEAR,7);
        calendar2.add(Calendar.YEAR,2);
        Date nextweekdate = calendar2.getTime();
        SimpleDateFormat formatter2 = new SimpleDateFormat("MM/dd/yyyy");
        String nextweekdate1 = formatter1.format(nextweekdate);

        /*while(tomorrow1.before(nextweekdate)){
          System.out.println(tomorrow1);
          tomorrow1.add(Calendar.DAY_OF_YEAR, 1);

          tomorrow1 = tomorrow1.plusDays(1);
        }*/


       Thread.sleep(5000);
        Screen s = new Screen();
        Pattern planSelect = new Pattern("src/resources/planSelect.png");
        Pattern interfaces = new Pattern("src/resources/Interfaces.png");
        Pattern downArrow = new Pattern("src/resources/downarrow.png");
        Pattern scrippsXML = new Pattern("src/resources/ScrippsXMLSchedule.png");
        Pattern promo = new Pattern("src/resources/promo.png");
        Pattern channelSelection = new Pattern("src/resources/channelSelection.png");
        Pattern HGTVHD = new Pattern("src/resources/HGTVHD.png");
        Pattern Channelsgroup = new Pattern("src/resources/Channelsgroup.png");
        Pattern channelsSelect = new Pattern("src/resources/channelsSelect.png");
        Pattern ForceExport = new Pattern("src/resources/ForceExport.png");
        Pattern OKScrippsXML = new Pattern("src/resources/OKScrippsXML.png");


        s.find(planSelect).hover();
        s.wait(2.0);
        s.click();
        s.wait(2.0);

        //s.type(Key.LEFT, 5);
        //s.click(s.wait(planSelect,10),10);
        s.wait(interfaces,10).hover();
        s.wait(2.0);
        s.find(downArrow).hover();
        s.wait(3.0);
        s.find(scrippsXML).hover();
        s.wait(2.0);
        s.click();

        s.wait(5.0);
        s.type(Key.TAB);


        s.paste(tomorrowsdate1);
        s.wait(2.0);
        s.type(Key.TAB);


        s.paste(nextweekdate1);
        s.wait(2.0);

        s.find(channelSelection).hover();
        s.wait(1.0);
        s.click(channelSelection);

        s.exists(Channelsgroup, 10);
        s.wait(HGTVHD,10).hover();
        s.wait(2.0);
        s.click();
        s.wait(1.0);
        s.type(Key.TAB);
        s.wait(1.0);
        s.type(Key.SPACE);
        s.find(channelsSelect).hover();
        s.wait(1.0);
        s.click();
        s.wait(2.0);
        s.click(ForceExport.targetOffset(-24, 0));
        s.wait(2.0);
        s.click(OKScrippsXML);
        s.wait(6.0);



        WebDriver driver = new FirefoxDriver();
        //WebDriver driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://mam-qa.scrippsnetworks.com/snidbit/home;jsessionid=FkxCuCfig67LyXg2tvdiTg__.snidbit-peer0");
        WebElement signin = driver.findElement(By.xpath("//a[@class='standAloneLink login']"));
        signin.click();
        WebElement userTextBox = driver.findElement(By.id("username"));
        userTextBox.clear();
        userTextBox.sendKeys("pnarayana");
        WebElement passwordTextBox = driver.findElement(By.id("password"));
        passwordTextBox.clear();
        passwordTextBox.sendKeys("pnarayana");
        //wait(2);
        WebElement loginButton = driver.findElement(By.xpath("//button[@value='Sign-In']"));
        loginButton.click();

        WebElement searchbox = driver.findElement(By.id("wunderbarSearch"));
        searchbox.click();
        searchbox.clear();
        searchbox.sendKeys(new_ep);
        WebElement searchEnter = driver.findElement(By.id("wunderbarSearchButton"));
        searchEnter.click();

        WebElement title = driver.findElement(By.xpath("//span[@class='showCode']"));
        System.out.println(title.getText());
        Assert.assertEquals(new_ep.toUpperCase(), title.getText());

        WebElement titleLink = driver.findElement(By.xpath(".//*[@class='title episodeTitle']"));
        System.out.println(titleLink.getText());
        titleLink.click();

        WebElement tableBody = driver.findElement(By.xpath(".//*[@id='airDates']/table/tbody"));

        //Date newerdate = tomorrow1.getTime() - nextweekdate.getTime();
        System.out.println(tableBody.getText().contains(tomorrowsdate1));


    }
}


