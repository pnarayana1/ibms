import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by 161744 on 10/6/2015.
 */
public class CreateGroupOfSlots {

    @Test

    public void ibmsLogin() throws Exception{
        org.sikuli.basics.Debug.setDebugLevel(3);

//Login to Citrix by using webdriver
// test

        WebDriver driver=new FirefoxDriver();
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //driver.get("http://snkxs024/Citrix/MetaFrame/auth/login.aspx");
        driver.get("http://snkxs024/Citrix/MetaFrame/site/default.aspx");
        WebElement userTextBox = driver.findElement(By.id("user"));
        userTextBox.clear();
        userTextBox.sendKeys("161744");
        WebElement passwordTextBox=driver.findElement(By.id("password"));
        passwordTextBox.clear();
        passwordTextBox.sendKeys("Scripps2@");
        // wait(2);
        WebElement loginButton = driver.findElement(By.name("login"));
        loginButton.click();
        driver.get("http://snkxs024/Citrix/MetaFrame/site/launch.ica?NFuse_Application=Citrix.MPS.App.SN_Citrix_Prod.Pilat+NET+QA&NFuse_AppFriendlyNameURLENcoded=Pilat+NET+QA");

        // Citrix/Firfox Error

        Screen s = new Screen();
        Pattern activateCitrix = new Pattern("src/resources/activateCitrix.png");
        Pattern allowAndRemember = new Pattern("src/resources/allowAndRemember.png");

        s.wait(5.0);
        s.click(s.exists(activateCitrix));
        s.wait(1.0);
        s.click(s.exists(allowAndRemember), 2*10);
        s.wait(5.0);

        // Login IBMS
        Pattern passWord = new Pattern("src/resources/passWord.png");
        s.exists(passWord, 150).hover();
        s.click(passWord.targetOffset(120,0));
        s.wait(5.0);
        s.type("pnarayana");
        s.wait(2.0);
        s.type(Key.TAB, Key.SHIFT);
        s.type("pnarayana");
        s.wait(1.0);
        s.type(Key.ENTER);
        s.wait(10.0);
        // Plan screen
        // Thread.sleep(5000);
        // Screen s = new Screen();
        // Pattern promo = new Pattern("src/resources/promo.png");
        //s.click(s.exists(promo),60);
        Pattern planSelect = new Pattern("src/resources/planSelect.png");
        // s.click(s.exists(planSelect),60);
        //s.wait(4.0);

        Pattern planGridSelect = new Pattern("src/resources/planGridSelect.png");
        Pattern  okPlanningGrid = new Pattern("src/resources/okPlanningGrid.png");
        Pattern helpPlan = new Pattern("src/resources/helpPlan.PNG");
        Pattern channelsPopup = new Pattern("src/resources/channelsPopup.png");
        Pattern channelsSelect = new Pattern("src/resources/channelsSelect.png");
        Pattern channelsCancel = new Pattern("src/resources/channelsCancel.png");
        /*while (s.exists(planGridSelect) == null){
            s.rightClick(s.exists(helpPlan.targetOffset(75, 0)));
            s.wait(2.0);
            s.click(s.exists(planSelect,5));
            s.wait(5.0);
        }*/
        s.click(s.exists(planSelect,8));
        //s.wait(5.0);
        s.click(s.exists(planGridSelect, 5));
        s.wait(2.0);
        s.type(Key.TAB);
        s.wait(2.0);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 2);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String tomorrowsdate = formatter.format(tomorrow);
        s.paste(tomorrowsdate);
        s.wait(5.0);






        s.wait(2.0);


        s.click(okPlanningGrid);
        s.wait(5.0);

        //Screen s = new Screen();
        //create group of slots
        Pattern groupName = new Pattern("src/resources/groupName.png");
        Pattern dateFrom = new Pattern("src/resources/dateFrom.png");
        Pattern time0630 = new Pattern("src/resources/time0630.png");
        Pattern greyarea = new Pattern("src/resources/greyarea.png");
        Pattern createGroup = new Pattern("src/resources/createGroup.png");
        Pattern create = new Pattern("src/resources/create.png");
        Pattern startTime = new Pattern("src/resources/startTime.png");
        s.wait(time0630, 5 * 30);
        s.rightClick(s.exists(greyarea).hover());
        s.wait(2.0);
        s.click(s.exists(createGroup));
        s.wait(groupName, 2 * 30);
        //s.click(groupName.targetOffset(86,0));
        s.type("a", Key.CTRL);
        s.wait(1.0);
        s.type("praveentest1");
        s.wait(2.0);
        s.click(dateFrom.targetOffset(75, 0));
        s.wait(2.0);
        s.type("a", Key.CTRL);
        //TO DO- Add date to filed and read date from file + 1 week ahead
        //get tomorrow's date
        Calendar calendar1 = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 2);
        Date tomorrow1 = calendar.getTime();
        SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
        String tomorrowsdate1 = formatter.format(tomorrow1);
        s.paste(tomorrowsdate);
        s.wait(5.0);
        s.click(startTime.targetOffset(-15, 29));
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.DOWN);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(2.0);
        s.type(Key.DOWN);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.DOWN);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.UP);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.UP);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.UP);
        s.wait(2.0);
        s.paste("08:00");
        s.click(create);

    }
}