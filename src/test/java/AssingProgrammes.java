import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by 161744 on 10/5/2015.
 */
public class AssingProgrammes {

    String Episode = "hhint802th";

    @Test

    public void ibmsLogin() throws Exception{
        org.sikuli.basics.Debug.setDebugLevel(3);



//Login to Citrix by using webdriver
// test

        WebDriver driver=new FirefoxDriver();
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //driver.get("http://snkxs024/Citrix/MetaFrame/auth/login.aspx");
        driver.get("http://snkxs024/Citrix/MetaFrame/site/default.aspx");
        WebElement userTextBox = driver.findElement(By.id("user"));
        userTextBox.clear();
        userTextBox.sendKeys("161744");
        WebElement passwordTextBox=driver.findElement(By.id("password"));
        passwordTextBox.clear();
        passwordTextBox.sendKeys("Scripps2@");
        // wait(2);
        WebElement loginButton = driver.findElement(By.name("login"));
        loginButton.click();
        driver.get("http://snkxs024/Citrix/MetaFrame/site/launch.ica?NFuse_Application=Citrix.MPS.App.SN_Citrix_Prod.Pilat+NET+QA&NFuse_AppFriendlyNameURLENcoded=Pilat+NET+QA");

        // Citrix/Firfox Error

        Screen s = new Screen();
        Pattern activateCitrix = new Pattern("src/resources/activateCitrix.png");
        Pattern allowAndRemember = new Pattern("src/resources/allowAndRemember.png");

        s.wait(2.0);
        s.click(s.exists(activateCitrix));
        s.wait(1.0);
        s.click(s.exists(allowAndRemember),
                2*10);
        s.wait(5.0);

        driver.quit();

        // Login IBMS
        Pattern passWord = new Pattern("src/resources/passWord.png");
        s.exists(passWord, 150).hover();
        s.click(passWord.targetOffset(120,0));
        s.wait(2.0);
        s.type("pnarayana");
        s.wait(2.0);
        s.type(Key.TAB, Key.SHIFT);
        s.type("pnarayana");
        s.wait(1.0);
        s.type(Key.ENTER);
        s.wait(10.0);

        Pattern planSelect = new Pattern("src/resources/planSelect.png");
        // s.click(s.exists(planSelect,8));
        //s.wait(4.0);

        Pattern planGridSelect = new Pattern("src/resources/planGridSelect.png");
        Pattern  okPlanningGrid = new Pattern("src/resources/okPlanningGrid.png");
        Pattern helpPlan = new Pattern("src/resources/helpPlan.PNG");
        Pattern channelsPopup = new Pattern("src/resources/channelsPopup.png");
        Pattern channelsSelect = new Pattern("src/resources/channelsSelect.png");
        Pattern channelsCancel = new Pattern("src/resources/channelsCancel.png");

       s.click(s.exists(planSelect,8));

        //s.wait(5.0);
        s.click(planGridSelect,5);

        s.wait(2.0);
        s.type(Key.TAB);
        s.wait(2.0);
        //s.click(s.exists(planGridSelect, 5));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 2);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String tomorrowsdate = formatter.format(tomorrow);
        s.paste(tomorrowsdate);
        s.wait(5.0);






        s.wait(2.0);


        s.click(okPlanningGrid);
        s.wait(5.0);

        //Screen s = new Screen();
        //create group of slots
        Pattern groupName = new Pattern("src/resources/groupName.png");
        Pattern dateFrom = new Pattern("src/resources/dateFrom.png");
        Pattern time0630 = new Pattern("src/resources/time0630.png");
        Pattern greyarea = new Pattern("src/resources/greyarea.png");
        Pattern createGroup = new Pattern("src/resources/createGroup.png");
        Pattern create = new Pattern("src/resources/create.png");
        Pattern startTime = new Pattern("src/resources/startTime.png");
        s.wait(time0630, 5 * 30);
        s.rightClick(s.exists(greyarea).hover());
        s.wait(2.0);
        s.click(s.exists(createGroup));
        s.wait(groupName, 2 * 30);
        //s.click(groupName.targetOffset(86,0));
        s.type("a", Key.CTRL);
        s.wait(1.0);
        s.type("praveentest1");
        s.wait(2.0);
        s.click(dateFrom.targetOffset(75, 0));
        s.wait(2.0);
        s.type("a", Key.CTRL);
        //TO DO- Add date to filed and read date from file + 1 week ahead
        //get tomorrow's date
        Calendar calendar1 = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 2);
        Date tomorrow1 = calendar.getTime();
        SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
        String tomorrowsdate1 = formatter.format(tomorrow1);
        s.paste(tomorrowsdate);
        s.wait(5.0);
        s.click(startTime.targetOffset(-15, 29));
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.DOWN);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(2.0);
        s.type(Key.DOWN);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.DOWN);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.UP);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.UP);
        s.wait(2.0);
        s.paste("08:00");
        s.wait(1.0);
        s.type(Key.UP);
        s.wait(2.0);
        s.paste("08:00");
        s.click(create);

        s.wait(5.0);

        //start of Assign programs test
      //  Screen s = new Screen();
      //  Thread.sleep(5000);
        Pattern plus2 = new Pattern("src/resources/plus2.png");
        Pattern treeAssign1 = new Pattern("src/resources/treeAssign1.png");
        Pattern treeAssign = new Pattern(("src/resources/treeAssign.png"));
        s.find(treeAssign).hover();
        s.rightClick(treeAssign.targetOffset(-5,-5));
        s.click(s.find(treeAssign1.exact()).hover(), 10);
        s.click(s.wait(plus2.targetOffset(-7, 0), 30));
        s.click(s.wait("src/resources/programmeQuery.png", 2));
        Pattern includeAKA = new Pattern("src/resources/includeAKA.png");
        s.wait(includeAKA, 10).hover();
        s.click(includeAKA.targetOffset(120, 0));
        s.type("hhint8");
        s.type(Key.ENTER);
        s.wait(5.0);
        s.exists("src/resources/rightarrow.png", 30);
        Pattern exitPQ = new Pattern("src/resources/exitPQ.png");
        s.click(exitPQ.targetOffset(0, -15));
        //s.exists("src/resources/exit.png", 30).hover();
        //s.click();
        s.click(s.wait(plus2.targetOffset(-7,0), 10));
        Pattern seriesPlus = new Pattern("src/resources/seriesPlus.png");
        s.click(s.wait(seriesPlus.targetOffset(-22,0), 10));
       // Pattern plusFolder = new Pattern("src/resources/plusfolder.png");
        s.click(s.wait(seriesPlus.targetOffset(-7, 15), 10));
        Pattern plusEpisode = new Pattern("src/resources/plusEpisode.png");
        Pattern date = new Pattern("src/resources/date.png");
       /* s.click(s.wait(plusEpisode.targetOffset(-7, 0), 10));
        s.dragDrop(plusEpisode.targetOffset(25, 25), date.targetOffset(0,50));
        Pattern undoOk = new Pattern("src/resources/undoOK.png");
        s.wait(undoOk.exact(),30);
        s.click(undoOk.targetOffset(30,0))*/;
        while (s.exists(plusEpisode.exact())!=null){
            System.out.println("inside while loop");
            if (s.exists(plusEpisode.exact())!=null) {
                System.out.println("inside if loop");
                s.click(s.wait(plusEpisode.exact().targetOffset(-7, 0), 10));
            }else{
              continue;
            }


        }
        Pattern checkEpisode = new Pattern("src/resources/checkEpisode.png");
        Pattern checkEpisodeBlue = new Pattern("src/resources/checkEpisodeBlue.png");

        Pattern OKScrippsXML = new Pattern("src/resources/OKScrippsXML.png");


        s.click(checkEpisode.exact());

        while (s.exists(checkEpisode.exact(), 5)!=null){
            if (s.exists(checkEpisode.exact()) != null) {
                s.keyDown(Key.CTRL);

                s.click(checkEpisode);

                s.keyUp(Key.CTRL);

            }else{
                continue;
            }
        }


        s.findAllByColumn(checkEpisodeBlue);

        s.dragDrop(checkEpisodeBlue, date.targetOffset(0, 15));

        s.wait(9.0);

        s.click(OKScrippsXML);


    }
}

